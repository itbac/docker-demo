#基础镜像
FROM openjdk:8-jdk-slim
#标签，作者
LABEL maintainer=bac
#复制jar包到镜像中的跟目录,命名为 app.jar
COPY target/*.jar   /app.jar
#设置时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone
#启动命令
ENTRYPOINT ["java","-jar","/app.jar"]