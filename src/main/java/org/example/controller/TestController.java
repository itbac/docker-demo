package org.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    /**
     * 127.0.0.1:8080/test/getTest
     * @return
     */
    @GetMapping("/getTest")
    public String getTest(){
        return "测试一下：" + System.currentTimeMillis();
    }
}
